<?php

class Voyager extends Data
{
  public static function exist($name)
  {
    $data = self::getData();
    $i = 0;
    $max = count($data->voyager);

    while ($i < $max) {
      if ($data->voyager[$i]->name === $name) {
        return true;
      }

      $i++;
    }

    return false;
  }

  public static function addVoyager($name, $visited_showplaces)
  {
    // incorrect name
    if (!$name) {
      return;
    }

    // voyager already exist
    if (self::exist($name)) {
      return;
    }

    // check visited places on existence
    foreach ($visited_showplaces as $i => $visited_showplace_object) {
      $showplace_name = $visited_showplace_object['name'];

      //if no exist
      if (!Showplace::exist($showplace_name)) {
        return;
      }
    }

    $voyager = [
      'name' => $name,
      'visited_showplaces' => $visited_showplaces,
    ];

    $data = self::getData();
    array_push($data->voyager, $voyager);
    self::saveData();
  }

  public static function getVoyagerByName($name)
  {
    $data = Data::getData();
    $voyagers = $data->voyager;

    foreach ($voyagers as $i => $voyager) {
      if ($voyager->name === $name) {
        return $voyager;
      }
    }

    return null;
  }

  public static function getAllVoyagers()
  {
    $data = Data::getData();
    $voyagers = $data->voyager;

    return $voyagers;
  }

  public static function countShowplaceRating($showplace_name)
  {
    $data = self::getData();
    $voyagers = $data->voyager;
    $total_rating = 0;
    $total_visitors = 0;

    foreach ($voyagers as $i => $voyager) {
      foreach ($voyager->visited_showplaces as $j => $showplace) {
        if ($showplace->name === $showplace_name) {
          $total_rating += $showplace->rating;
          $total_visitors += 1;
        }
      }
    }

    $rating = ($total_visitors > 0) ? $total_rating / $total_visitors : -1;

    return $rating;
  }
}
