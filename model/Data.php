<?php

class Data
{
  private static $data;

  protected static function getData()
  {
    return self::$data;
  }

  protected static function saveData()
  {
    $json = json_encode(self::$data);
    file_put_contents('../data.json', $json);
  }

  public static function load()
  {
    $json = file_get_contents('../data.json');
    self::$data = json_decode($json);
  }
}
