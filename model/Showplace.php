<?php

class Showplace extends Data
{
  public static function exist($name)
  {
    $data = self::getData();
    $i = 0;
    $max = count($data->showplace);

    while ($i < $max) {
      if ($data->showplace[$i]->name === $name) {
        return true;
      }

      $i++;
    }

    return false;
  }

  public static function addShowplace($city_name, $showplace_name, $distance)
  {
    if (!City::cityExists($city_name)) {
      return;
    }

    $data = self::getData();

    // check unique
    if (self::exist($showplace_name)) {
      return;
    }

    $showplace = [
      'city' => $city_name,
      'name' => $showplace_name,
      'distance' => $distance
    ];

    array_push($data->showplace, $showplace);
    self::saveData();
  }

  public static function getShowplaceByName($name)
  {
    $data = Data::getData();
    $showplaces = $data->showplace;

    foreach ($showplaces as $i => $showplace) {
      if ($showplace->name === $name) {
        return $showplace;
      }
    }

    return null;
  }

  public static function getAllShowplaces()
  {
    $data = Data::getData();
    $showplaces = $data->showplace;

    return $showplaces;
  }
}
