<?php

class City extends Data
{
  /**
   * create new city
   * @param $name string new city name
   */
  public static function addCity($name)
  {
    $city = [
      "name" => $name,
    ];
    $data = self::getData();

    // check unique
    $unique = true;
    $i = 0;
    $max = count($data->city);

    while ($unique && $i < $max) {
      if ($data->city[$i]->name === $name) {
        $unique = false;
      }

      $i++;
    }

    if (!$unique) {
      return;
    }

    array_push($data->city, $city);
    self::saveData();
  }

  /**
   * to check city on existence
   * @param $name city name
   * @return true if exists, false otherwise
  */
  public static function cityExists($name)
  {
    $data = self::getData();
    $i = 0;
    $max = count($data->city);

    while ($i < $max) {
      if ($data->city[$i]->name === $name) {
        return true;
      }

      $i++;
    }

    return false;
  }

  public static function getAllCities()
  {
    $data = self::getData();
    return $data->city;
  }
}
