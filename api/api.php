<?php

require_once('../model/Data.php');
require_once('../model/City.php');
require_once('../model/Showplace.php');
require_once('../model/Voyager.php');

Data::load();

if (isset($_GET['action'])) {
  $action = $_GET['action'];
} elseif (isset($_POST['action'])) {
  $action = $_POST['action'];
}

//print_r($_POST);

//$action = [
//  'get-showplaces',
//  'which-cities-voyager-visited',
//  'city-visitors',
//  'add-city',
//  'add-showplace',
//  'add-voyager'
//][0];

switch ($action) {
  case 'add-city':
    City::addCity($_POST['city-name']);
    break;

  case 'add-showplace':
    Showplace::addShowplace($_POST['city-name'], $_POST['showplace-name'], $_POST['distance']);
    break;

  case 'add-voyager':
    Voyager::addVoyager($_POST['voyager-name'], []);
    break;

  // в каких городах какие достопримечательности
  case 'get-showplaces':
    $showplaces = Showplace::getAllShowplaces();
    $cities = [];

    foreach ($showplaces as $i => $showplace) {
      $city_name = $showplace->city;

      if (!isset($cities[$city_name])) {
        $cities[$city_name] = [];
      }

      $showplace_rating = Voyager::countShowplaceRating($showplace->name);

      array_push($cities[$city_name], [
        'name' => $showplace->name,
        'distance' => $showplace->distance,
        'rating' => $showplace_rating
      ]);
    }

    function cmp($a, $b)
    {
      $a_rating = (int)$a["rating"];
      $b_rating = (int)$b["rating"];

      if ($a_rating === $b_rating) {
        return 0;
      }
      return ($a_rating > $b_rating) ? -1 : 1;
    }

    foreach ($cities as $i => $city) {
      usort($city, 'cmp');
      $cities[$i] = $city;
    }

    echo json_encode($cities);
    break;

  case 'get-cities':
    $cities = City::getAllCities();
    echo json_encode($cities);
    break;

  case 'get-voyagers':
    $voyagers = Voyager::getAllVoyagers();
    echo json_encode($voyagers);
    break;

  // какие города посетил путешественник
  case 'which-cities-voyager-visited':
    $voyager_name = $_GET['voyager-name'];
    $voyager = Voyager::getVoyagerByName($voyager_name);
    $cities = [];

    foreach ($voyager->visited_showplaces as $i => $visited_showplace_object) {
      $showplace_name = $visited_showplace_object->name;
      $showplace = Showplace::getShowplaceByName($showplace_name);
      $city_name = $showplace->city;

      if (array_search($city_name, $cities) === false) {
        array_push($cities, $city_name);
      }
    }

    echo json_encode($cities);
    break;

  // какие путешественники побывали в городе
  case 'city-visitors':
    $city_name = $_GET['city-name'];
    $city_visitors = [];
    $voyagers = Voyager::getAllVoyagers();

    foreach ($voyagers as $i => $voyager) {
      $visited_showplaces = $voyager->visited_showplaces;

      foreach ($visited_showplaces as $j => $visited_showplace_object) {
        $showplace_name = $visited_showplace_object->name;
        $showplace = Showplace::getShowplaceByName($showplace_name);

        if ($showplace->city === $city_name) {
          array_push($city_visitors, $voyager->name);
          break;
        }
      }
    }

    echo json_encode($city_visitors);
    break;
}
